from src.app.support_func import prefix


def cum():
    return None

def linearsearch(lst, elem):
    for i in range(len(lst)):
        cum()
        if lst[i] == elem:
            return i
            break

def binarysearch(lst, elem):
    first = 0
    last = len(lst) - 1
    index = -1
    while (first <= last) and (index == -1):
        cum()
        mid = (first + last) // 2
        if lst[mid] == elem:
            index = mid
        else:
            if elem < lst[mid]:
                last = mid - 1
            else:
                first = mid + 1
    return index


def naivesearch(txt, Subtxt):
    len_subtxt = len(Subtxt)
    len_txt = len(txt)

    for i in range(len_txt-len_subtxt+1):
        status = 1
        for j in range(len_subtxt):
            cum()
            if txt[i+j] != Subtxt[j]:
                status = 0
                break
        if j == len_subtxt-1 and status != 0:
            return i


def KMP(txt, Subtxt):
    index = -1
    f = prefix(Subtxt)
    k = 0
    for i in range(len(txt)):
        cum()
        while k > 0 and (Subtxt[k] != txt[i]):
            k = f[k-1]
        if Subtxt[k] == txt[i]:
            k = k + 1
        if k == len(Subtxt):
            index = i - len(Subtxt) + 1
            break
    return index