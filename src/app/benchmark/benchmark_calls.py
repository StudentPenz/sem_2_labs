from src.app.benchmark import benchmark_func
import matplotlib.pyplot as plt
from src.app.benchmark.bench_constants import LEN_ARRAY, theoretical_complexity, \
    dict_algorithms, name_algorithms
from numpy import poly1d as np_poly1d, polyfit as np_polyfit

def calls(choice):
    for e in choice:
        if e == '1' or e == '2':
            fig = plt.figure()
            fig.set_size_inches(12, 9, forward=True)
            calls_lst = benchmark_func.lst_operation_num(dict_algorithms[e])
            plt.subplot()
            plt.title(f"Сложность({name_algorithms[e]})")
            trend = np_poly1d(np_polyfit(LEN_ARRAY, calls_lst, 1))
            plt.plot(LEN_ARRAY, trend(LEN_ARRAY), 'indigo', linestyle='-.', label='Тренд практической сложности')
            plt.plot(LEN_ARRAY, calls_lst, 'red', label="Практическая сложность")
            plt.plot(LEN_ARRAY, theoretical_complexity[e](LEN_ARRAY), linestyle='--',
                     label="Теоретическая сложность(худшая)")
            plt.xlabel('длина массива, шт')
            plt.ylabel('Кол-во операций, шт')
            plt.legend()
            plt.savefig(f"graphs/{name_algorithms[e]}.png")
        else:
            fig = plt.figure()
            fig.set_size_inches(12, 9, forward=True)
            calls_lst = benchmark_func.lst_operation_str(dict_algorithms[e])
            plt.subplot()
            plt.title(f"Сложность({name_algorithms[e]})")
            trend = np_poly1d(np_polyfit(LEN_ARRAY, calls_lst, 1))
            plt.plot(LEN_ARRAY, trend(LEN_ARRAY), 'indigo', linestyle='-.', label='Тренд практической сложности')
            plt.plot(LEN_ARRAY, calls_lst, 'red', label="Практическая сложность")
            plt.plot(LEN_ARRAY, theoretical_complexity[e](LEN_ARRAY), linestyle='--',
                     label="Теоретическая сложность(худшая)")
            plt.xlabel('длина массива, шт')
            plt.ylabel('Кол-во операций, шт')
            plt.legend()
            plt.savefig(f"graphs/{name_algorithms[e]}.png")
