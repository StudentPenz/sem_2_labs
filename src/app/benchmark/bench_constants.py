from src.app.search_base import naivesearch, linearsearch, binarysearch, KMP
from math import log

#def quadratic_complexity(array):
 #   return [e**2 for e in array]


def log_n_complexity(array):
    return [log(e, 2) for e in array]


def n_complexity(array):
    return [e for e in array]


LEN_ARRAY = [i for i in range(10000, 100001, 10000)]


name_algorithms = {
    '1': 'Линейный',
    '2': 'Бинарный поиск',
    '3': 'Наивный',
    '4': 'Кнута-Морриса-Пратта'}

dict_algorithms = {
    '1': linearsearch,
    '2': binarysearch,
    '3': naivesearch,
    '4': KMP}

theoretical_complexity = {
    '1': n_complexity,
    '2': log_n_complexity,
    '3': n_complexity,
    '4': n_complexity}


