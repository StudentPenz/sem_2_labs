from src.app.search_base import linearsearch, binarysearch, naivesearch, KMP
import cProfile
import pstats
import random
import string


def lst_operation_num(func):
    result = []
    for i in range(10000, 100001, 10000):
        lst = [(random.uniform(-64, 63)) for _ in range(i)]
        search = lst[-5]
        stats = cProfile.Profile()
        stats.enable()
        func(lst, search)
        stats.disable()
        with open(f"output.txt", 'w') as log_file_stream:
            p = pstats.Stats(stats, stream=log_file_stream)
            p.strip_dirs().sort_stats().print_stats('cum')
        f = open('output.txt')
        lines = [line.strip() for line in f]
        lines_1 = [line for line in lines if
                   line not in '' and line.split()[0].isnumeric()]
        for line in lines_1:
            if line.find('cum') != -1:
                result.append(int(line.split('   ')[0]))
        f.close()
    return result


def lst_operation_str(func):
    result = []
    for i in range(10000, 110000, 10000):
        words = string.ascii_lowercase
        lst = ''.join(random.choice(words) for _ in range(i))
        search = lst[-5000: -4950]
        stats = cProfile.Profile()
        stats.enable()
        func(lst, search)
        stats.disable()
        with open(f"output.txt", 'w') as log_file_stream:
            p = pstats.Stats(stats, stream=log_file_stream)
            p.strip_dirs().sort_stats().print_stats('cum')
        f = open('output.txt')
        lines = [line.strip() for line in f]
        lines_1 = [line for line in lines if
                    line not in '' and line.split()[0].isnumeric()]
        for line in lines_1:
            if line.find('cum') != -1:
                result.append(int(line.split('   ')[0]))
        f.close()
    return result
