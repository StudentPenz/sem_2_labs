from src.app.benchmark import benchmark_calls


def main():
    print("Выберите нужные алгоритмы:\n",
          "1 - Линейный поиск\n",
          "2 - Бинарный поиск\n",
          "3 - Наивный поиск\n",
          "4 - Кнута-Морриса-Пратта\n")

    choice = (input("Введите номера алгоритмов через пробел >> \n")).split()
    try:
        benchmark_calls.calls(choice)
    except KeyError:
        raise ValueError('Некорректный ввод')
    print("Результаты работы программы можно наблюдать на сохраненных графиках выбранных алгоритмов.")


if __name__ == "__main__":
    main()

